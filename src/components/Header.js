import React from "react";
import { Link } from "react-router-dom";
import App from "../App";
import Cars from "../Cars";

const Header = () => {
  return (
    <>
      <div>O Brabo</div> 
      <nav>
        <Link to='/' component={<App/>}>Go App</Link> | {" "}
        <Link to='cars' component={<Cars />} >Go Home</Link>
      </nav>
    </>
  )
}

export default Header