import React from "react";
import { useParams } from "react-router-dom";

const Car = () => {
  const params = useParams();
  return (
    <div>{params.car}</div>
  )
}

export default Car