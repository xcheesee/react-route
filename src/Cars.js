import React from 'react'
import { Outlet } from 'react-router-dom'

export default function Cars() {
    return (
        <div>
            <h1>Hello Cars</h1>
            <Outlet />
        </div>
    )
}