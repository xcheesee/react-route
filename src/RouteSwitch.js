import { BrowserRouter, Routes, Route } from 'react-router-dom'
import App from './App'
import Cars from './Cars'
import Car from './components/Car'
import Layout from './Layout'
import NotFound from './components/NotFound'

export default function RouteSwitch () {
  return (
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<Layout />}>
          <Route index element={<App />} />
          <Route path='cars' element={<Cars />}>
            <Route path=':car' element={<Car />}/>
          </Route>
        </Route>
        <Route path='*' element={<NotFound />} />
      </Routes>
    </BrowserRouter>
  );
}